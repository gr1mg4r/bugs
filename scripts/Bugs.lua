local ability = require "necro.game.system.Ability"
local affectorItem = require "necro.game.item.AffectorItem"
local attack = require "necro.game.character.Attack"
local collision = require "necro.game.tile.Collision"
local commonShrine = require "necro.game.data.object.CommonShrine"
local components = require "necro.game.data.Components"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local event = require "necro.event.Event"
local health = require "necro.game.character.Health"
local inventory = require "necro.game.item.Inventory"
local minimapTheme = require "necro.game.data.tile.MinimapTheme"
local object = require "necro.game.object.Object"
local player = require "necro.game.character.Player"

local ecs = require "system.game.Entities"


components.register {
	brokenGlasses = {},
	crashArmor = {},
	desync = {},
	rollback = {
		components.field.int32("turnID", -1),
	},
}


customEntities.extend {
	name = "TorchOverflow",
	template = customEntities.template.item(),
	components = {
		itemSlot = { name = "torch" },
		sprite = { texture = "mods/bugs/torch_overflow.png" },
		itemSpawnFlyawayOnPickup = { text = "Torch of Overflowwwwwwwwwwwwwwwwwwwwwwwwwwwwwww" },
		lightSource = {},
		lightSourceRadial = {
			innerRadius = 1280,
			outerRadius = 1281,
			brightness = 8363108,
		},
		visionRadial = { radius = 1280 },
	},
}


customEntities.extend {
	name = "HeadBrokenGlasses",
	template = customEntities.template.item(),
	components = {
		itemSlot = { name = "head" },
		sprite = {
			texture = "mods/bugs/head_broken_glasses.png",
			width = 30,
		},
		itemSpawnFlyawayOnPickup = { text = "Broken Glasses" },
		itemAttackBaseDamageIncrease = { increase = 2 },
		bugs_brokenGlasses = {},
	}
}

event.renderEffects.add("squareTown", "spriteCorrection", function (ev)
	if affectorItem.isItemActive("bugs_brokenGlasses") then
		for entity in ecs.entitiesWithComponents {"visibleByTelepathy", "sprite"} do
			entity.sprite.textureShiftX = entity.sprite.textureShiftX + 42
		end
	end
end)


customEntities.extend {
	name = "ArmorCrash",
	template = customEntities.template.item(),
	components = {
		itemSlot = { name = "body" },
		sprite = { texture = "mods/bugs/armor_crash.png" },
		itemSpawnFlyawayOnPickup = { text = "Crash Armor" },
		itemArmorBodySpriteRow = {},
		bugs_crashArmor = {},
	}
}

event.holderTakeDamage.add("crashArmor", {order = "armorLate", filter = "bugs_crashArmor"}, function (ev)
	if ev.damage >= health.getCurrentHealth(ev.holder) then
		ecs.resetAll()
	end
	ev.damage = ev.damage / 2
end)


customEntities.extend {
	name = "WeaponDaggerDesync",
	template = customEntities.template.item("weapon_dagger"),
	components = {
		sprite = { texture = "mods/bugs/dagger_desync.png" },
		itemSpawnFlyawayOnPickup = { text = "Desync Dagger" },
		bugs_desync = {},
	}
}

local knockbackValues = { 0, 0, 0, 0, 1/6, 1/2, 1, 1, 1, 1, 2, 2, 3, 99 }

event.holderDealDamage.add("desync", {order = "knockback", filter = "bugs_desync"}, function (ev)
	ev.knockback = knockbackValues[math.random(#knockbackValues)]
	ev.damage = ev.damage + math.random(6) - 1
end)


customEntities.extend {
	name = "FeetRollback",
	template = customEntities.template.item(),
	components = {
		itemSlot = { name = "feet" },
		sprite = { texture = "mods/bugs/feet_rollback.png" },
		itemSpawnFlyawayOnPickup = { text = "Rollback Socks" },
		bugs_rollback = {},
	}
}

event.clientAddInput.add("rollbackBoots", {order = "turnID", sequence = 1}, function (ev)
	if not ability.Flag.check(ev.flags, ability.Flag.ACTION_QUEUE) then
		return
	end
	local item = affectorItem.getItem(player.getPlayerEntity(ev.playerID), "bugs_rollback")
	if item then
		if item.bugs_rollback.turnID == -1 then
			item.bugs_rollback.turnID = ev.turnID
		else
			ev.turnID = item.bugs_rollback.turnID - 1
		end
	end
end)

event.gameStateLevel.add("resetRollbackSocks", "statusEffects", function (ev)
	for item in ecs.entitiesWithComponents {"bugs_rollback"} do
		item.bugs_rollback.turnID = -1
	end
end)

commonShrine.registerShrine("bugs", {
	friendlyName = { name = "?%$!???$?!!?#?????#??%%???$?????" },
	rowOrder = { z = 1 },
	sprite = { texture = "mods/bugs/shrine_bugs.png" },
})

local slotMapping = {
	torch = "bugs_TorchOverflow",
	head = "bugs_HeadBrokenGlasses",
	body = "bugs_ArmorCrash",
	weapon = "bugs_WeaponDaggerDesync",
	feet = "bugs_FeetRollback",
}

event.shrine.add("bugs", "bugs", function (ev)
	for slot, targetType in pairs(slotMapping) do
		local item = inventory.getItemInSlot(ev.interactor, slot)
		if item then
			object.convert(item, targetType)
		end
	end
	ev.entity.shrine.active = false
end)

event.gameStateLevel.add("bugs", "apparition", function (ev)
	if currentLevel.isBoss() then
		object.spawn("bugs_ShrineOfBugs", 0, 0)
	end
end)

local hasModShop, ModShop = pcall(require, "ModShop.ItemPool")
if (hasModShop) then
	ModShop.add("bugs_TorchOverflow")
	ModShop.add("bugs_HeadBrokenGlasses")
	ModShop.add("bugs_ArmorCrash")
	ModShop.add("bugs_WeaponDaggerDesync")
	ModShop.add("bugs_FeetRollback")
end
